define borg::backup (
  String  $backup_path               = '/',
  String  $archive_name              = undef,
  String  $archive_path              = undef,
  Variant[String, Undef] $passphrase = undef,
  Array   $exclude                   = [ '/proc', '/sys', '/run' ],
  Boolean $prune                     = true,
  String  $compression               = 'auto,lz4',
  Integer $daily                     = 7,
  Integer $weekly                    = 4,
  Integer $monthly                   = 6,
  Integer $yearly                    = 0,
  Boolean $log                       = false,
  Variant $type                      = [ 'borg', 'sshfs' ],
  String  $sshfs_mount               = '/mnt',
  String  $user                      = 'root',
  String  $cron_time                 = '0 1 * * *',
  Boolean $envvar_relocated_repo     = false,
  Boolean $envvar_unknown_repo       = false ) {


  file {
    "/usr/local/sbin/borg-${name}":
      content => epp('borg/backups.epp', {
        'name'                  => $name,
        'backup_path'           => $backup_path,
        'archive_name'          => $archive_name,
        'archive_path'          => $archive_path,
        'passphrase'            => $passphrase,
        'exclude'               => $exclude,
        'prune'                 => $prune,
        'compression'           => $compression,
        'daily'                 => $daily,
        'weekly'                => $weekly,
        'monthly'               => $monthly,
        'yearly'                => $yearly,
        'log'                   => $log,
        'type'                  => $type,
        'sshfs_mount'           => $sshfs_mount,
        'user'                  => $user,
        'cron_time'             => $cron_time,
        'envvar_relocated_repo' => $envvar_relocated_repo,
        'envvar_unknown_repo'   => $envvar_unknown_repo,
      }),
      owner   => $user,
      group   => $user,
      mode    => '0700';

    "/etc/cron.d/borg-${name}":
      ensure  => file,
      content => "${cron_time} ${user} /usr/local/sbin/borg-${name}\n",
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }
}
