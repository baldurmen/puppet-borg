class borg::config {

  package {
    [ 'borgbackup',
      'sshfs' ]:
        ensure => present;
  }

  # To log borg exit codes
  file { '/var/log/borg':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755';
  }
}
